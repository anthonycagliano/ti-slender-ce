/*
 *--------------------------------------
 * Program Name:
 * Author:
 * License:
 * Description:
 *--------------------------------------
*/

/* Keep these headers */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <tice.h>

/* Standard headers (recommended) */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <decompress.h>

#include <graphx.h>
#include <fileioc.h>
/* Put your function prototypes here */

/* Put all your globals here */
#include "slendersprites.h"
#include "data/position.h"
#include "data/entity.h"
#include "data/map.h"
#include "data/trig.h"
#include "globals.h"

#include "engines/rendering.h"
#include "engines/spawning.h"
#include "engines/movement.h"

void main(void) {
    uint8_t key = 0;
    uncompressed = gfx_MallocSprite(116, 60);
    ti_CloseAll();
    boot_ClearVRAM();
    gfx_Begin();
    gfx_SetDefaultPalette(gfx_8bpp);
    gfx_SetClipRegion(0, 0, 320, 240);
    gfx_SetTransparentColor(255);
    gfx_SetDrawBuffer();
    // Draw Splash Screen
    gfx_ZeroScreen();
    dzx7_Standard(title_compressed, uncompressed);
    scaled = gfx_MallocSprite(uncompressed->width, uncompressed->height<<1);
    scaled->width = uncompressed->width;
    scaled->height = uncompressed->height<<1;
    gfx_ScaleSprite(uncompressed, scaled);
    gfx_Sprite(scaled, 160 - (uncompressed->width / 2), 30);
    free(scaled);
    dzx7_Standard(toplay_compressed, uncompressed);
    gfx_Sprite(uncompressed, 160 - (uncompressed->width / 2), 150);
    dzx7_Standard(toquit_compressed, uncompressed);
    gfx_Sprite(uncompressed, 160 - (uncompressed->width / 2), 175);
    gfx_BlitBuffer();
    do {
        key = os_GetCSC();
        if(key == sk_Clear){
            gfx_End();
            boot_ClearVRAM();
            prgm_CleanUp();
            os_ClrHomeFull();
            return;
        }
    } while(key != sk_Enter);
    spawn_SpawnPlayer();
    spawn_GenerateTerrain();
    spawn_PlacePages();
    spawn_SpawnAI();
    key = 0;
    do {
        key = os_GetCSC();
        switch(key){
            case sk_2nd:
                player->data.player.flashlightOn = !player->data.player.flashlightOn;
                break;
            case sk_Left:
                player->location.rotation -= 5;
                break;
            case sk_Right:
                player->location.rotation += 5;
                break;
            case sk_Up:
                if(player->data.player.moving < 1) player->data.player.moving++;
                break;
            case sk_Down:
                if(player->data.player.moving > -1) player->data.player.moving--;
                break;
        }
        if(player->data.player.moving == 1) {
            player->location.position[loc_x] += player->location.vectors[loc_x];
            player->location.position[loc_y] += player->location.vectors[loc_y];
        } else if( player->data.player.moving == -1){
            player->location.position[loc_x] -= player->location.vectors[loc_x];
            player->location.position[loc_y] -= player->location.vectors[loc_y];
        }
        if(player->data.player.batterylife == 0) player->data.player.flashlightOn = false;
        gfx_renderScreen();
    } while(key != sk_Clear);
    free(uncompressed);
    ti_CloseAll();
    gfx_End();
    boot_ClearVRAM();
    prgm_CleanUp();
    os_ClrHomeFull();
}

/* Put other functions here */
