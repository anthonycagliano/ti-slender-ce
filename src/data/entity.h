
//
//  entity.h
//  TI-Slender-CE
//
//  Created by Anthony Cagliano on 1/15/18.
//  Copyright © 2018 ClrHome Productions. All rights reserved.
//

#ifndef entity_h
#define entity_h

#include "position.h"

enum EntityTypes {
    player_n,
    slender_n,
    page_n
};

typedef struct {
    uint8_t pages;
    uint8_t sanity;
    uint8_t stamina;
    int8_t moving;
    uint16_t batterylife;
    bool flashlightOn;
    bool staminaAble;
    bool isScared;
    bool isSprinting;
} Player_t;

typedef struct {
    uint8_t instaKillChance;
    uint8_t teleportChance;
    uint8_t walkingSpeed;
} Slender_t;

typedef struct {
    uint8_t teleportChance;
    uint8_t walkingSpeed;
} Proxy_t;

typedef struct {
    uint8_t type;
    Position_t location;
    union data {
        Player_t player;
        Slender_t slender;
        Proxy_t proxy;
    } data;
} EntityData_t;

#endif /* entity_h */
