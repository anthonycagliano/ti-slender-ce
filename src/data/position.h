
//
//  position.h
//  TI-Slender-CE
//
//  Created by Anthony Cagliano on 1/15/18.
//  Copyright © 2018 ClrHome Productions. All rights reserved.
//

#ifndef position_h
#define position_h

typedef struct {
    int16_t position[2];
    int16_t vectors[2];
    uint8_t rotation;
    uint32_t distance;
} Position_t;


#endif /* position_h */
