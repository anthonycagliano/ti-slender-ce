//
//  map.h
//  TI-Slender-CE
//
//  Created by Anthony Cagliano on 1/15/18.
//  Copyright © 2018 ClrHome Productions. All rights reserved.
//

#ifndef map_h
#define map_h

#include "position.h"
#define VISUAL_RANGE 400
#define loc_x 0
#define loc_y 1

enum EnvironmentTypes {
    tree,
    bigtree,
    silo,
    truck,
    car,
    rock,
    deadtree,
    watertower,
    outhouse
};


typedef struct {
    uint8_t type;
    Position_t location;
} MapData_t;

#endif /* map_h */
