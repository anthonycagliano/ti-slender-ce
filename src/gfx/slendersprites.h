// Converted using ConvPNG
// This file contains all the graphics sources for easier inclusion in a project
#ifndef __slendersprites__
#define __slendersprites__
#include <stdint.h>

#define slendersprites_transparent_color_index 255

#define title_width 116
#define title_height 60
#define title_size 6962
extern uint8_t title_compressed[457];
#define toplay_width 107
#define toplay_height 14
#define toplay_size 1500
extern uint8_t toplay_compressed[206];
#define toquit_width 109
#define toquit_height 15
#define toquit_size 1637
extern uint8_t toquit_compressed[204];
#define bigtree_width 64
#define bigtree_height 64
#define bigtree_size 4098
extern uint8_t bigtree_compressed[839];
#define deadtree_width 64
#define deadtree_height 64
#define deadtree_size 4098
extern uint8_t deadtree_compressed[132];
#define flashlight_width 64
#define flashlight_height 64
#define flashlight_size 4098
extern uint8_t flashlight_compressed[510];
#define flashlight_dying_width 64
#define flashlight_dying_height 64
#define flashlight_dying_size 4098
extern uint8_t flashlight_dying_compressed[383];
#define rock_width 64
#define rock_height 64
#define rock_size 4098
extern uint8_t rock_compressed[811];
#define silo_width 64
#define silo_height 64
#define silo_size 4098
extern uint8_t silo_compressed[26];
#define slenderman_width 32
#define slenderman_height 64
#define slenderman_size 2050
extern uint8_t slenderman_compressed[243];
#define tree_width 24
#define tree_height 96
#define tree_size 2306
extern uint8_t tree_compressed[226];
#define car_width 64
#define car_height 64
#define car_size 4098
extern uint8_t car_compressed[519];
#define truck_width 64
#define truck_height 64
#define truck_size 4098
extern uint8_t truck_compressed[468];
#define tower_width 64
#define tower_height 64
#define tower_size 4098
extern uint8_t tower_compressed[323];
#define outhouse_width 64
#define outhouse_height 64
#define outhouse_size 4098
extern uint8_t outhouse_compressed[208];
#define page_width 24
#define page_height 24
#define page_size 578
extern uint8_t page_compressed[54];
#define sizeof_slendersprites_pal 512
extern uint16_t slendersprites_pal[256];

#endif
