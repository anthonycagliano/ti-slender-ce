// Converted using ConvPNG
#include <stdint.h>
#include "slendersprites.h"

// 8 bpp image
uint8_t toquit_compressed[204] = {
 0x6D,0x20,0x0F,0x00,0x6F,0x00,0x71,0xFF,0x00,0x10,0x0A,0x51,0x29,0x07,0x33,0x6C,0xDC,0x47,0x0A,0xCB,0x11,0xFF,0x10,0x8B,0x11,0x96,0x10,0x26,0x6C,0x27,0x20,0x00,
 0x8E,0x8A,0x09,0xE2,0x24,0xE2,0x3C,0xE4,0x12,0x5D,0x8D,0x5A,0x8B,0x2F,0xE7,0x04,0x06,0x8E,0x1F,0xB9,0x07,0xEE,0x1C,0x3F,0x3A,0x6B,0xE3,0x56,0x46,0x8E,0x22,0x65,
 0xE2,0x1A,0xA2,0x61,0xE3,0x65,0x26,0x73,0x19,0x8E,0x8A,0x08,0x88,0x68,0x99,0x43,0xE3,0x3D,0x63,0x6C,0x42,0xC7,0x33,0x52,0x12,0x8B,0x4E,0x8D,0x6C,0x8B,0x36,0xE3,
 0x03,0x61,0x4B,0xA8,0x6C,0xF1,0xE4,0x84,0xAE,0x45,0x07,0x1F,0x6C,0x9E,0x3A,0x26,0x06,0x08,0xA2,0x6C,0x86,0xD7,0x32,0x36,0x15,0x6C,0xB9,0x3F,0xC2,0xC9,0x38,0xF7,
 0x60,0xA8,0xC6,0x62,0x67,0x37,0x74,0x8F,0xB3,0x28,0x40,0xB2,0xA2,0x23,0x6C,0xE3,0x25,0xCF,0xA0,0x04,0x8D,0x6C,0x8A,0xF2,0x19,0xCE,0xF4,0x2A,0x37,0x78,0x31,0x94,
 0xA2,0x96,0x26,0x31,0xAA,0x23,0xC2,0xA2,0x29,0x68,0xFF,0x1D,0x00,0x16,0x91,0x12,0xC7,0x7B,0x4D,0x6C,0x6D,0x3C,0x47,0x52,0xC4,0xC4,0xC7,0x10,0xC1,0x6C,0x53,0x80,
 0xC0,0x95,0xAE,0x10,0x7F,0x29,0x01,0xEF,0x00,0x00,0x00,0x80,
};
