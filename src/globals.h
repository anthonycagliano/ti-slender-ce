//
//  globals.h
//  TI-Slender-CE
//
//  Created by Anthony Cagliano on 1/15/18.
//  Copyright © 2018 ClrHome Productions. All rights reserved.
//

#ifndef globals_h
#define globals_h

MapData_t MapData[100] = {0};
EntityData_t EntityData[11] = {0};
EntityData_t *player = &EntityData[0];
EntityData_t *slenderman = &EntityData[1];

gfx_sprite_t* uncompressed;
gfx_sprite_t* scaled = NULL;

#define MAP_XMAX 128
#define MAP_YMAX 128

#define LIGHT_TIME 1000

#endif

