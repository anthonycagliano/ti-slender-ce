//
//  spawning.h
//  TI-Slender-CE
//
//  Created by Anthony Cagliano on 1/15/18.
//  Copyright © 2018 ClrHome Productions. All rights reserved.
//

#ifndef spawning_h
#define spawning_h

#include "movement.h"
#include "../data/map.h"

void spawn_PlaceObjectRandom(uint8_t index, uint8_t type);
void spawn_PlaceObjectOn(uint8_t dest, uint8_t source);


void spawn_SpawnPlayer(void){
    Position_t *playerpos = &player->location;
    player->type = player_n;
    playerpos->position[loc_x] = randInt(0, MAP_XMAX) * 256;
    playerpos->position[loc_y] = randInt(0, MAP_YMAX) * 256;
    playerpos->rotation = 0;
    move_RotToVectors(&player->location);
    player->data.player.pages = 0;
    player->data.player.sanity = 255;
    player->data.player.stamina = 255;
    player->data.player.batterylife = LIGHT_TIME;
    player->data.player.flashlightOn = true;
    player->data.player.staminaAble = true;
    player->data.player.isScared = false;
    player->data.player.isSprinting = false;
}

void spawn_GenerateTerrain(void){
    uint8_t i;
    for(i = 0; i <= 91; i++)
        spawn_PlaceObjectRandom(i, tree);
    for(i = 92; i <= 100; i++)
        spawn_PlaceObjectRandom(i, i-91);
}


void spawn_PlacePages(void){
    uint8_t i;
    for(i = 0; i <= 7; i++)
        spawn_PlaceObjectOn(i+2, randInt(0, 100));
}


void spawn_SpawnAI(void){
    slenderman->location.position[loc_x] = randInt(0, MAP_XMAX) * 256;
    slenderman->location.position[loc_y] = randInt(0, MAP_YMAX) * 256;
    slenderman->type = slender_n;
}

void spawn_PlaceObjectRandom(uint8_t index, uint8_t type){
    int16_t randx, randy;
    uint8_t check;
    bool approved = false;
    MapData_t *entry = &MapData[index];
    entry->type = type;
    do {
        approved = true;
        randx = randInt(0, MAP_XMAX);
        randy = randInt(0, MAP_YMAX);
        for(check = 0; check < index; check++){
            MapData_t *temp = &MapData[check];
            int16_t tempx = temp->location.position[loc_x] / 256;
            int16_t tempy = temp->location.position[loc_y] / 256;
            if( (tempx == randx) && (tempy == randy)) {
                approved = false;
                break;
            }
        }
    } while( !approved);
    entry->location.position[loc_x] = randx * 256;
    entry->location.position[loc_y] = randy * 256;
}

void spawn_PlaceObjectOn(uint8_t dest, uint8_t source){
    MapData_t *sourceobject = &MapData[source];
    uint8_t xloc = sourceobject->location.position[loc_x] / 256;
    uint8_t yloc = sourceobject->location.position[loc_y] / 256;
    EntityData_t *destobject = &EntityData[dest];
    destobject->type = page_n;
    destobject->location.position[loc_x] = xloc * 256;
    destobject->location.position[loc_y] = yloc * 256;
}


#endif
