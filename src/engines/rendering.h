//
//  rendering.h
//  TI-Slender-CE
//
//  Created by Anthony Cagliano on 1/15/18.
//  Copyright © 2018 ClrHome Productions. All rights reserved.
//

#ifndef rendering_h
#define rendering_h
#include "../data/position.h"
#include "../data/entity.h"
#include "../data/map.h"

enum RenderTypes {
    r_tree,
    r_bigtree,
    r_silo,
    r_truck,
    r_car,
    r_rock,
    r_deadtree,
    r_watertower,
    r_outhouse,
    r_slender,
    r_page
};

typedef struct {
    uint8_t type;
    uint32_t distance;
    int8_t angleOffset;
} RenderArray_t;

int compareDistance(RenderArray_t *a, RenderArray_t *b){
    int a_dist = a->distance;
    int b_dist = b->distance;
    if (a_dist < b_dist)
        return 1;  // Return -1 if you want ascending, 1 if you want descending order.
    else if (a_dist > b_dist)
        return -1;   // Return 1 if you want ascending, -1 if you want descending order.
    
    return 0;
}

void misc_GetDistance(Position_t *p, Position_t *object){
    int16_t deltax = abs((p->position[loc_x]/256) - (object->position[loc_x]/256));
    int16_t deltay = abs((p->position[loc_y]/256) - (object->position[loc_y]/256));
    uint32_t deltax_squared = deltax * deltax;
    uint32_t deltay_squared = deltay * deltay;
    uint32_t distance = deltax_squared + deltay_squared;
    object->distance = distance;
}

void gfx_renderScreen(void){
    
    uint24_t batteryPercent;
    uint8_t i, number = 0;
    RenderArray_t RenderArray[100] = {0};
    // black out screen
    // double flashlight sprite size and render

    for( i = 0; i <= (sizeof(MapData)/sizeof(MapData_t)); i++){
        MapData_t *object = &MapData[i];
        RenderArray_t *r_object = &RenderArray[number];
        if(object->type){
            misc_GetDistance(&player->location, &object->location);
            if( object->location.distance < VISUAL_RANGE ){
                
                int16_t deltax = (object->location.position[loc_x] - player->location.position[loc_x]) / 256;
                int16_t deltay = (object->location.position[loc_y] - player->location.position[loc_y]) / 256;
                int8_t deltaRot = atan( deltay / deltax );
                deltaRot -= player->location.rotation;
            
                if( abs(deltaRot) <= 45 ){
                    r_object->type = object->type;
                    r_object->angleOffset = deltaRot;
                    r_object->distance = object->location.distance;
                    number++;
                }
            }
        }
    }
    for( i = 1; i <= (sizeof(EntityData)/sizeof(EntityData_t)); i++){
        EntityData_t *object = &EntityData[i];
        RenderArray_t *r_object = &RenderArray[number];
        if(object->type){
            misc_GetDistance(&player->location, &object->location);
            if( object->location.distance < VISUAL_RANGE ){
                
                int8_t deltax = object->location.position[loc_x] - player->location.position[loc_x];
                int8_t deltay = object->location.position[loc_y] - player->location.position[loc_y];
                int8_t deltaRot = atan( deltay / deltax );
                deltaRot -= player->location.rotation;
                
                if( abs(deltaRot) <= 45 ){
                    r_object->type = object->type + 9;
                    r_object->angleOffset = deltaRot;
                    r_object->distance = object->location.distance;
                    number++;
                }
            }
        }
    }

/* CAN WE IMPLEMENT SOME SORT OF SHUNTING YARD TO ORDER ITEMS BY DISTANCE */
    if (number > 1)
        qsort(&RenderArray[0], number, sizeof(RenderArray_t), &compareDistance);

    gfx_ZeroScreen();
    /* RENDER FLASHLIGHT MASK */
    if( player->data.player.flashlightOn ){
        uint8_t scale = 216;
        if(player->data.player.batterylife > (LIGHT_TIME>>1))
            dzx7_Standard(flashlight_compressed, uncompressed);
        else
            dzx7_Standard(flashlight_dying_compressed, uncompressed);
        scaled = gfx_MallocSprite(scale, scale);
        scaled->width = scale;
        scaled->height = scale;
        gfx_ScaleSprite(uncompressed, scaled);
        gfx_Sprite(scaled, 160 - (scaled->width>>1), 260 - scaled->height);
        free(scaled);
        scaled = NULL;
        player->data.player.batterylife--;
    }
    for( i = 0; number && i <= number; i++){
        RenderArray_t *object = &RenderArray[i];
        if( object->type ){
            uint8_t minscale[2];
            uint8_t scale[2];
            uint16_t xcoord, ycoord;
        
            switch( object->type){
                case r_tree:
                    dzx7_Standard(tree_compressed, uncompressed);
                    break;
                case r_bigtree:
                    dzx7_Standard(bigtree_compressed, uncompressed);
                    break;
                case r_silo:
                    dzx7_Standard(silo_compressed, uncompressed);
                    break;
                case r_truck:
                    dzx7_Standard(truck_compressed, uncompressed);
                    break;
                case r_car:
                    dzx7_Standard(car_compressed, uncompressed);
                    break;
                case r_rock:
                    dzx7_Standard(rock_compressed, uncompressed);
                    break;
                case r_deadtree:
                    dzx7_Standard(deadtree_compressed, uncompressed);
                    break;
                case r_watertower:
                    dzx7_Standard(tower_compressed, uncompressed);
                    break;
                case r_outhouse:
                    dzx7_Standard(outhouse_compressed, uncompressed);
                    break;
                case r_slender:
                    dzx7_Standard(slenderman_compressed, uncompressed);
                    break;
                case r_page:
                    dzx7_Standard(page_compressed, uncompressed);
                    break;
            }
            minscale[loc_x] = uncompressed->width;
            minscale[loc_y] = uncompressed->height;
            scale[loc_x] = (VISUAL_RANGE - object->distance) * minscale[loc_x] / VISUAL_RANGE;
            scale[loc_y] = (VISUAL_RANGE - object->distance) * minscale[loc_y] / VISUAL_RANGE;
            scale[loc_x] += uncompressed->width;
            scale[loc_y] += uncompressed->height;
            scaled = gfx_MallocSprite(scale[loc_x], scale[loc_y]);
            if(scaled != NULL){
                scaled->width = scale[loc_x];
                scaled->height = scale[loc_y];
                gfx_ScaleSprite(uncompressed, scaled);
                xcoord = (object->angleOffset + 45) * 320 / 91;
                ycoord = 240 - scaled->height;
                gfx_TransparentSprite(scaled, xcoord - (scaled->width/2), ycoord);
                free(scaled);
                scaled = NULL;
            }
        }
    }
    gfx_BlitBuffer();
}

#endif /* rendering_h */
