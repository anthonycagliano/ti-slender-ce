//
//  movement.h
//  TI-Slender-CE
//
//  Created by Anthony Cagliano on 1/15/18.
//  Copyright © 2018 ClrHome Productions. All rights reserved.
//

#ifndef movement_h
#define movement_h

void move_RotToVectors(Position_t* location){
    uint8_t angle = location->rotation;
    location->vectors[loc_x] = sinLUT[angle + 64];
    location->vectors[loc_y] = sinLUT[angle];
}

#endif
